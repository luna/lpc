# lpc

lunaping but in crystal

original lunaping: https://github.com/slice/lp

compared to unix ping this:
 - has minimum of 3 colors if you count default text color
 - go above `icmp_seq` of 65535 so you know when you did 4 billion pings to `1.1.1.1`
    - it wont work when you get to 15 quintillion sorry

## Installation

```bash
git clone https://gitlab.com/lnmds/lpc
cd lpc
shards build
cp bin/lpc somewhere_where_you_put_binaries/lpc
```

## Usage

```bash
sudo lpc 1.1.1.1
```

(sudo is required since kernel doesnt give
permissions for icmp sockets right away)

## Contributing

1. Fork it (<https://gitlab.com/lnmds/lpc/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request

## Contributors

- [lnmds](https://gitlab.com/lnmds) Luna Mendes - creator, maintainer
