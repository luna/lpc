require "signal"
require "colorize"

require "icmp"

addresses = ARGV

class Stats
  property address : String
  property count = 0_u64
  property success = 0_u64
  property fail = 0_u64
  property fail_count = 0_u64

  def initialize(@address)
  end

  # generate a report in the end.
  def to_s(io)
    if success == 0
      err_rate = 0
    else
      err_rate = (fail / success) * 100
    end
    io << "#{@address}:\
    total: #{@count} \
    success: #{@success} \
    fail: #{@fail}
    error rate: #{err_rate}%"
  end

  def report_bool(st_bool)
    if st_bool
      @success += 1

      # use -1 as a signaler of "a success happened after
      # X failures"
      if @fail_count <= 0
        @fail_count = 0
      else
        @fail_count = -1
      end
    else
      @fail += 1
      @fail_count += 1
    end
  end
end

# this is a flag for the main loop of yield() down this script
keep_loop = true

# keep this array to print results when we finish off
all_stats = [] of Stats

Signal::INT.trap do
  keep_loop = false
end

def status_from_req(request)
  case request.status
  when :invalid_response
    "FAIL".colorize :red
  when :valid_response
    rtt = request.roundtrip_time
    color = :green

    if rtt > 170
      color = :yellow
    elsif rtt > 300
      color = :red
    end

    "#{rtt}ms".colorize color
  end
end

def notify_success(stats)
  `notify-send yay`
end

def notify_failure(stats)
  `notify-send eaugh`
end

def maybe_notify(stats)
  if stats.fail_count == -1
    notify_success stats
  elsif stats.fail_count > 2
    notify_failure stats
  end
end

def start_ping(all_stats, address)
  # create a stats object for this address
  stats = Stats.new address
  all_stats.insert(all_stats.size, stats)

  loop do
    begin
      ICMP::Ping.new(address).ping() do |request|
        st_bool = request.status == :valid_response

        # get a status line for our puts call
        status = status_from_req request
        stats.report_bool(st_bool)

        puts "#{address} [#{stats.count}] => #{status}"

        maybe_notify(stats)
        Fiber.yield
      end
    rescue ex
      err = "ping err #{address}: #{ex.message}".colorize :red
      puts err
      stats.fail += 1
    ensure
      # always increase count, at least
      stats.count += 1
      sleep 1
    end
  end
end

if addresses.size == 0
  puts "put addresses as args"
  exit
end

addresses.each do |address|
  puts address

  spawn do
    start_ping all_stats, address
  end

  sleep 500.millisecond
end

# main loop is here. after spawning each pinger fiber,
# we just keep yield()ing so that they get actually called. (in this case
# the yield is based around the sleep call, replacing this by a yield
# causes very bad cpu usage)

# the keep_loop global var is to handle C-C's correctly and
# stop the program, plus printing a report
while keep_loop
  sleep 100.millisecond
end

puts "end"

all_stats.each do |stats|
  puts stats
end
